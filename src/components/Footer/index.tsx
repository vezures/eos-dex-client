import * as React from "react"


export default class Footer extends React.Component<{}, {}> {
  render() {
    return (
      <footer id="app-footer">
        Powered by Vezures &amp; CryptoControl.io &middot; 2018 &copy; All Rights Reserved <br />
        Terms &amp; Conditions  &middot; Privacy Policy &middot; Contact
        {/* Looking to clone this EOS DEX?<br /> */}
      </footer>
    )
  }
}
