import * as _ from "underscore"
import * as React from "react"
import * as ReactHighcharts from "react-highcharts"
// import { any } from "../../../../../libraries/serverAPI"


interface IProps {
  data: any// any[]
  coinSlug: string
}


export default class CoinChart extends React.Component<IProps, {}> {
  chart: any



  _getRegressionColor(data: any) {
    if (data[0][1] > data[1][1]) return "rgba(255, 99, 82, 0.5)"
    return "rgba(0, 182, 40, 0.5)"
  }


  _getRegression(data: any[]) {
    const xs: any[] = []
    const ys: any[] = []
    let minX = 1E100
    let maxX = -1E100

    data.forEach(function (k: any[]) {
      if (k[0] < minX) minX = k[0]
      if (k[0] > maxX) maxX = k[0]
      xs.push(k[0])
      ys.push(k[1])
    })

    const reg: any = {}
    this._lineFit(xs, ys, reg)
    return [
      [minX, reg.slope * minX + reg.intercept],
      [maxX, reg.slope * maxX + reg.intercept]
    ]
  }


  // utility functions
  _lineFit(xs: number[], ys: number[], rV: any) {
    rV.slope = 0.0
    rV.intercept = 0.0
    rV.rSquared = 1.0 // assume perfection

    if (xs.length < 2) return false
    if (xs.length !== ys.length) return false

    const N = xs.length
    const sumX = this._sumFunc(xs)
    const sumY = this._sumFunc(ys)
    const funcSq = (i) => i * i
    const funcSq2 = (i, j) => i * j

    const sumXx = this._sumFunc(xs, funcSq)
    const sumYy = this._sumFunc(ys, funcSq)
    const sumXy = this._sumFunc(this._zip(xs, ys), funcSq2)

    rV.slope = ((N * sumXy) - (sumX * sumY)) / (N * sumXx - (sumX * sumX))
    rV.intercept = (sumY - rV.slope * sumX) / N
    rV.rSquared = Math.abs((rV.slope * (sumXy - (sumX * sumY) / N)) / (sumYy - ((sumY * sumY) / N)))
    return true
  }


  _sumFunc(arr: any[], func?: (a: number, b?: number) => number) {
    let total = 0

    arr.forEach(function (k: any, i: number) {
      if (_.isArray(k)) {
        if (!func) k = k[0] + k[1]
        else k = func(k[0], k[1])
      } else if (func) k = func(k)

      total += k
    })

    return total
  }


  _zip(arr1: any[], arr2: any[]) {
    const rV: any[] = []
    for (let i = 0; i < arr1.length; i++) rV.push([arr1[i], arr2[i]])
    return rV
  }


  getChartConfig(data: any[]) {
    const formatFiat = function (val: number) {
      if (val >= 1) {
        if (val >= 100000) return Math.round(val).toLocaleString()
        return val.toFixed(2)
      } else {
        if (val < 0.000001) return val.toPrecision(2)
        return val.toFixed(6)
      }
    }

    const formatCrypto = function (val: number) {
      if (val >= 1000) return Math.round(val).toLocaleString()
      if (val >= 1) return val.toFixed(8)

      if (val < 0.00000001) return val.toPrecision(4)
      return val.toFixed(8)
    }


    const tooltipFormatFiat = function () {
      const val = formatFiat(this.y)
      return `<span style="color: ${this.color}">\u25CF</span> ${this.series.name}: <b>${val}</b><br/>`
    }

    const tooltipFormatCrypto = function () {
      const val = formatCrypto(this.y)
      return `<span style="color: ${this.color}">\u25CF</span> ${this.series.name}: <b>${val}</b><br/>`
    }

    const series: any[] = [{
      data: _.map(data, d => { return [d.time, d.priceUSD] }),
      showInLegend: false,
      name: "Price USD",
      tooltip: { pointFormatter: tooltipFormatFiat },
    }]

    if (this.props.coinSlug !== "bitcoin") {
      series.push({
        data: _.map(data, d => { return [d.time, d.priceBTC] }),
        name: "Price BTC",
        lineWidth: 1,
        yAxis: 1,
        showInLegend: false,
        fillOpacity: 0,
        tooltip: { pointFormatter: tooltipFormatCrypto }
      })
    }

    series.push({
      data: _.map(data, d => { return [d.time, d.volumeUSD24h] }),
      name: "24h Vol USD",
      type: "area",
      step: true,
      yAxis: 2,
      lineWidth: 0,
      showInLegend: false,
      grouping: false,
      showEmpty: false,
      offset: 2,
      tooltip: { pointFormatter: tooltipFormatFiat },
      className: "chc-volume-series",
      // tooltip: {pointFormatter: tooltipFormatCrypto}
    })

    const regressionData = this._getRegression(_.map(data, d => { return [d.time, d.priceUSD] }))
    series.push({
      name: "Regression Line",
      data: regressionData,
      showInLegend: false,
      marker: { enabled: false },
      yAxis: 0,
      lineWidth: 2,
      lineColor: this._getRegressionColor(regressionData),
      fillOpacity: 0,
      dashStyle: "shortdash"
    })

    const volumeColorVal = "100" // (Services.config.theme === "light" ? "100" : "170")

    const colors = [
      "#2CCDEE",
      `rgba(${volumeColorVal}, ${volumeColorVal}, ${volumeColorVal}, .7)`
    ]

    const btcChartColor = "rgba(255, 188, 52, .9)"
    const isMobileXs = false // App.isMobileXS(),
    const rangeButtons = [{
      type: "day",
      count: 1,
      text: "1d"
    }, {
      type: "day",
      count: 7,
      text: "7d"
    }, {
      type: "month",
      count: 1,
      text: "1m"
    }, {
      type: "month",
      count: 3,
      text: "3m"
    }, {
      type: "month",
      count: 6,
      text: "6m"
    }, {
      type: "year",
      count: 1,
      text: "1y"
    }, {
      type: "all",
      text: "All"
    }]

    const rangeCookieName = "chartRangeBtn"
    const rangeCookieValue = 6// parseInt(Cookies.get(rangeCookieName)),
    const defaultRangeBtn = 6
    const selectedRangeBtn = isNaN(rangeCookieValue) ? defaultRangeBtn : rangeCookieValue

    // Yellow color for bitcoin
    if (this.props.coinSlug !== "bitcoin") colors.splice(1, 0, btcChartColor)

    return {
      colors: colors,
      chart: {
        global: {
          useUTC: false
        },
        lang: {
          rangeSelectorZoom: ""
        },
        type: "area",
        zoomType: "x",
        className: "chc",
        animation: false,
        margin: [0, 0, 0, 0],
        spacingTop: 0,
        marginTop: 0,
        borderWidth: 0,
        spacing: 0,
        events: {
          load: (event) => {
            this.chart = event.target
            if (selectedRangeBtn !== defaultRangeBtn) {
              this.chart.rangeSelector.clickButton(selectedRangeBtn, true)
            }
          }
        },
      },
      credits: {
        enabled: false
      },
      scrollbar: {
        enabled: false,
        liveRedraw: false,
        margin: 0,
      },
      title: {
        text: "Data by <span>coinmarketcap.com</span>",
        // !App.isTouchDevice() ? "Data by <span>coinmarketcap.com</span>" : ",
        floating: true,
        align: "left",
        x: 20,
        y: 26,
        marginTop: 0,
        style: {
          fontSize: "11px"
        }
      },
      subtitle: {
        text: ""
      },
      plotOptions: {
        series: {
          animation: false,
          threshold: null,
          lineOpacity: 0.2,
          fillOpacity: 0.2,
          lineWidth: 1.5,
          states: {
            hover: {
              enabled: false
            }
          },
          dataGrouping: {
            enabled: false
          }
        },
        area: {
          boostThreshold: 0,
          turboThreshold: 0,
          getExtremesFromAll: true,
        },
        column: {
          grouping: false,
        }
      },
      rangeSelector: {
        verticalAlign: "top",
        // allButtonsEnabled: true,
        y: 12,
        x: -20,
        buttonPosition: {
          align: "right"
        },
        inputPosition: {
          align: "left"
        },
        // floating: true,
        // y: 10,
        buttons: rangeButtons,
        inputEnabled: false, // it supports only days
        selected: defaultRangeBtn, // all,
      },
      navigator: {
        adaptToUpdatedData: false,
        outlineWidth: 0,
        series: [
          { data: _.map(data, d => { return [d.time, d.priceUSD] }) },
        ],
        yAxis: {
          gridLineWidth: 0,
          lineWidth: 0,
          tickWidth: 0,
        },
        xAxis: {
          gridLineWidth: 0,
          lineWidth: 0,
          tickWidth: 0,
          labels: {
            enabled: !isMobileXs,
          }
        },
        handles: {
          enabled: !isMobileXs,
        },
        enabled: true,
        height: isMobileXs ? 0 : 40,
        // enabled: !App.isTouchDevice(),
      },
      exporting: {
        enabled: false
      },
      tooltip: {
        shared: true,
        split: false,
        crosshairs: true,
        shadow: false,
        padding: 5,
        borderWidth: 0,
        borderRadius: 0,
        snap: "1/2",
      },
      xAxis: {
        events: {
          afterSetExtremes: e => {
            if (typeof (e.rangeSelectorButton) !== "undefined") {
              // const btnIndex = parseInt(rangeButtons.findIndex(b => {
              //   return b.count === e.rangeSelectorButton.count && b.type === e.rangeSelectorButton.type
              // }))
              // if (btnIndex === defaultRangeBtn && !isNaN(rangeCookieValue)) {
              //   Cookies.remove(rangeCookieName)
              // } else if (btnIndex !== rangeCookieValue) {
              //   Cookies.set(rangeCookieName, btnIndex, { expires: 30 });
              // }
            }
            if (e.dataMin !== e.min || e.dataMax !== e.max) {
              const min = Math.round(e.min), max = Math.round(e.max)
              // this.redrawChart(min, max)
            }
          }
        },
        minRange: 24 * 3600 * 1000,
      },
      yAxis: [{
        floor: 0,
        maxPadding: 0,
        labels: {
          align: "right",
          x: -5
        },
      }, {
        floor: 0,
        maxPadding: 0,
        opposite: false,
        labels: {
          align: "left",
          x: 5
        }
      }, {
        floor: 0,
        maxPadding: 0,
        opposite: false,
        top: "80%",
        height: "20%",
        labels: {
          enabled: false
        },
      }],
      series: series
    }
  }


  render() {
    if (!process.env.CLIENT || window.innerWidth < 600) return <div />

    const config = this.getChartConfig(this.props.data)
    return (<ReactHighcharts config={config} />)
  }
}
