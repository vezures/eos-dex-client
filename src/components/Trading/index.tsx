import * as React from "react"

import AllTokens from "./AllTokens"
import Charts from "./Charts"
import MyBalances from "./MyBalances"
import OpenOrders from "./OpenOrders"
import Orderbook from "./OrderBook"
import PairInfo from "./PairInfo"
import PlaceBuyOrder from "./PlaceBuyOrder"
import PlaceSellOrder from "./PlaceSellOrder"
import TradeHistory from "./TradeHistory"
import NewsWidget from "./NewsWidget"


export default class TradingIndex extends React.Component<{}, {}> {
  render() {
    return (
      <div className="app row">
        <div className="columns"><PairInfo /></div>

        <div className="columns medium-8">
          <div className="row">
            <div className="columns"><Charts /></div>

            <div className="columns medium-6"><PlaceBuyOrder /></div>
            <div className="columns medium-6"><PlaceSellOrder /></div>

            <div className="columns medium-6"><TradeHistory /></div>
            <div className="columns medium-6"><OpenOrders /></div>
            <div className="columns medium-6"><MyBalances /></div>
            <div className="columns medium-6"><AllTokens /></div>
          </div>
        </div>
        <div className="columns medium-4">
          <Orderbook />
          <NewsWidget />
        </div>
      </div>
    )
  }
}
