import * as React from "react"

import MainHeader from "./MainHeader"
import MenuHeader from "./MenuHeader"
import AccountHeader from "./AccountHeader"


export default class Header extends React.Component<{}, {}> {
  render() {
    return (
      <header id="app-header">
        <MainHeader />
        <AccountHeader />
        <MenuHeader />
      </header>
    )
  }
}
