import * as React from "react"


// see something like https://tools.cryptokylin.io/#/tools/stake
export default class AccountHeader extends React.Component<{}, {}> {
  render() {
    return (
      <header className="subheader" id="account-header">
        <div className="title">username@owner</div>
        <div className="title">scattter connected</div>
        <div className="title red">remove identity</div>
        <div id="wallet-details" className="title">Wallet Details</div>
      </header>
    )
  }
}
