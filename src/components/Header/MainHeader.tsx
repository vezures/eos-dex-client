import * as React from "react"


export default class MainHeader extends React.Component<{}, {}> {
  render() {
    return (
      <header className="subheader" id="main-header">
        <div className="title logo">Vezures DEX</div>
        <div id="wallet-details" className="title">Wallet Details</div>
      </header>
    )
  }
}
