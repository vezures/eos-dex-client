import * as React from "react"
import { Link } from "react-router-dom"


export default class MenuHeader extends React.Component<{}, {}> {
  render() {
    return (
      <header className="subheader" id="menu-header">
        <div className="title"><Link to="/">/Trade</Link></div>
        <div className="title"><Link to="/margin">/Margin</Link></div>
        <div className="title"><Link to="/wallet">/Wallet</Link></div>
        <div className="title"><Link to="/blockexplorer">/Blockexplorer</Link></div>
        <div className="title"><Link to="/listing">/Listing</Link></div>
        <div className="title"><Link to="/api">/Api</Link></div>
        <div className="title"><Link to="/about">/About</Link></div>
      </header>
    )
  }
}
