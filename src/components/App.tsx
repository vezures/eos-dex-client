import * as React from "react"
import {
  HashRouter as Router,
  Redirect,
  Switch,
  Route
} from "react-router-dom"

import "./../assets/sass/App.black.sass"

import Footer from "./Footer"
import Header from "./Header"
import Trading from "./Trading"
import ComingSoon from "./ComingSoon"


export default class App extends React.Component<{}, {}> {
  render() {
    return (
      <Router>
        <div id="app-container">
          <Header />
            <main id="app-root">
              <Switch>
                <Redirect exact from="/" to="/trade/eos/vez" />
                <Route path="/trade/:base/:quote" component={Trading} />
              </Switch>

              <Route exact path="/about" component={ComingSoon} />
              <Route exact path="/wallet" component={ComingSoon} />
              <Route exact path="/listing" component={ComingSoon} />
              <Route path="/blockexplorer" component={ComingSoon} />
              <Route path="/margin" component={ComingSoon} />
            </main>
          <Footer />
        </div>
      </Router>
    )
  }
}
