import * as React from "react"


export default class ComingSoon extends React.Component<{}, {}> {
  render() {
    return (
      <div className="row">
        <h2>This page is coming soon</h2>
      </div>
    )
  }
}
